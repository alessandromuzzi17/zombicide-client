﻿// ﻿using System;
// using UnityEngine;
// using System.Collections;
// using UnityEngine.Events;
//
// // public class DragBehaviour : MonoBehaviour {
// // 	float distance = 10;
// // 	void OnMouseDrag(){
// // 		Vector3 mousePosition = new Vector3 (Input.mousePosition.x, Input.mousePosition.y, distance);
// // 		Vector3 objPosition = Camera.main.ScreenToWorldPoint (mousePosition);
// // 		transform.position = objPosition;
// // 	}
// // }
// public class DragBehaviour : MonoBehaviour {
//
// 	#region Private Properties
//
// 	private float zPosition;
// 	private Vector3 offset;
// 	private bool isDragging;
//
// 	#endregion
//
// 	#region Inspector Variables
//
// 	public Camera mainCamera;
// 	[Space]
// 	[SerializeField]
// 	public UnityEvent OnBeginDrag;
// 	[SerializeField]
// 	public UnityEvent OnEndDrag;
//
// 	#endregion
// 	
// 	private void Start()
// 	{
// 		zPosition = transform.position.y + 2;
// 	}
//
// 	private void Update()
// 	{
// 		if (isDragging)
// 		{
// 			Vector3 position = new Vector3(Input.mousePosition.x, zPosition, Input.mousePosition.y);
// 			// var pos = mainCamera.ScreenToWorldPoint(position + new Vector3(offset.x, 0, offset.y));
// 			var pos = position + new Vector3(offset.x, 0, offset.y);
// 			pos.y = zPosition;
// 			transform.position = pos;
// 			Debug.Log($"Pos: {transform.position}");
// 		}
// 	}
//
// 	private void OnMouseDown()
// 	{
// 		if (!isDragging)
// 		{
// 			BeginDrag();
// 		}
// 	}
//
// 	private void OnMouseUp()
// 	{
// 		EndDrag();
// 	}
//
// 	public void BeginDrag()
// 	{
// 		OnBeginDrag.Invoke();
// 		isDragging = true;
// 		offset = mainCamera.WorldToScreenPoint(transform.position) - new Vector3(Input.mousePosition.x, 0, Input.mousePosition.y);
// 		offset.y = 0;
// 	}
// 	public void EndDrag()
// 	{
// 		OnEndDrag.Invoke();
// 		isDragging = false;
// 	}
// }
