using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using UnityEngine;

public class ZombicideClient
{
    private AppMain appMain;
    private Socket sender;
    private IPAddress remoteIpAddress;
    private Timer pollingTimer;
    private bool connected;
    
    public ZombicideClient(AppMain _appMain, IPAddress ipAddress = null)
    {
        appMain = _appMain;
        connected = false;
        if (ipAddress == null)
        {
            // Establish the remote endpoint for the socket.  
            // This example uses port 11000 on the local computer.  
            IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());
            
            remoteIpAddress = ipHostInfo.AddressList.First(it => it.AddressFamily == AddressFamily.InterNetwork);
        }
        StartClient();
    }
    
    public void StartClient()
    {
        // Connect to a remote device.  
        try
        {
            // Create a TCP/IP  socket.  
            //var remoteHostEntry = Dns.GetHostEntry(Dns.GetHostName()); 
            var remoteHostEntry = Dns.GetHostEntry("casamuzzi.ddns.net");
            var remoteIpAddr = remoteHostEntry.AddressList[0];
            var remoteEndPoint = new IPEndPoint(remoteIpAddr, 11000);
            sender = new Socket(remoteIpAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

            // Connect the socket to the remote endpoint. Catch any errors.  
            try
            {
                sender.Connect(remoteEndPoint);
                connected = true;

                Console.WriteLine("Socket connected to {0}", sender.RemoteEndPoint);

                #region Old Example Code

                // // Data buffer for incoming data.  
                // byte[] bytes = new byte[1024];
                // ...
                // // Encode the data string into a byte array.  
                // byte[] msg = Encoding.ASCII.GetBytes("{\"operation\": \"new_user\", \"name\": \"Pippo\"}");
                //
                // // Send the data through the socket.  
                // int bytesSent = sender.Send(msg);
                //
                // // Receive the response from the remote device.  
                // int bytesRec = sender.Receive(bytes);
                // Console.WriteLine("Echoed test = {0}",
                //     Encoding.ASCII.GetString(bytes, 0, bytesRec));

                #endregion
            }
            catch (ArgumentNullException ane)
            {
                Console.WriteLine("ArgumentNullException : {0}", ane.ToString());
            }
            catch (SocketException se)
            {
                Console.WriteLine("SocketException : {0}", se.ToString());
            }
            catch (Exception e)
            {
                Console.WriteLine("Unexpected exception : {0}", e.ToString());
            }

        }
        catch (Exception e)
        {
            Console.WriteLine(e.ToString());
        }
    }

    public void StopClient()
    {
        if (sender != null)
        {
            // Release the socket.  
            sender.Shutdown(SocketShutdown.Both);
            sender.Close();
        }
    }

    #region Request Methods
    
    /**
       Richiesta
       {
	       "operation": "get_table"
       }
       Risposta
       {
	       "rows": "2",
       "cols": "1",
	       “tiles”: [“1V up”, “3V right” ]
       }
     */
    public Tuple<Size, string[]> GetTable()
    {
        var requestJson = new JObject();
        requestJson["operation"] = "get_table";
        var jsonContent = Send(requestJson.ToString());
        var jsonObject = JObject.Parse(jsonContent);
        return new Tuple<Size, string[]>(new Size((int)jsonObject["cols"], (int)jsonObject["rows"]), ((JArray)jsonObject["tiles"]).ToObject<string[]>());
    }

    /**
       Richiesta
       {
	       "operation": "new_user",
	       "name": "Pippo",
	       "player_name": "Jhon",
       }
       Risposta
       Caso positivo:
       {
	       "result": "success"
       }
       Caso negativo:
       {
	       "result": "failed"
       }
     */
    public KeyValuePair<bool, Player> SendNewPlayer(Player player)
    {
        var firstMacAddress = NetworkInterface
            .GetAllNetworkInterfaces()
            .FirstOrDefault(nic => nic.OperationalStatus == OperationalStatus.Up && nic.NetworkInterfaceType != NetworkInterfaceType.Loopback)?
            .GetPhysicalAddress().ToString();
        player.MacAddress = firstMacAddress;
        var requestJson = new JObject
        {
            ["operation"] = "new_user",
            ["name"] = "",
            ["player"] = JsonConvert.SerializeObject(player)
        };
        
        try
        {
            var jsonContent = Send(requestJson.ToString());
            var jsonObject = JObject.Parse(jsonContent);

            var result = ((string) jsonObject["result"]) == "success";
            return new KeyValuePair<bool, Player>(result, JsonConvert.DeserializeObject<Player>((string)jsonObject["player"]));
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
        }
        
        return new KeyValuePair<bool, Player>(false, null);
    }

    #region Object Management Methods

    /**
        Richiesta
        {
	        "operation": "move_object",
	        "uid": 15248,
	        "from_coord_x": "10",
	        "from_coord_z": "12"
	        "to_coord_x": "20",
	        "to_coord_z": "12"
        }
        Risposta
        Caso positivo:
        {
	        "result": "success"
        }
        Caso negativo:
        {
	        "result": "failed"
        }
     */
    public bool SendObjectMove(string uid, Vector3 fromPos, Vector3 toPos)
    {
        var requestJson = new JObject();
        requestJson["operation"] = "move_object";
        requestJson["uid"] = uid;
        requestJson["from_coord_x"] = fromPos.x;
        requestJson["from_coord_z"] = fromPos.z;
        requestJson["to_coord_x"] = toPos.x;
        requestJson["to_coord_z"] = toPos.z;
        
        try
        {
            var jsonContent = Send(requestJson.ToString());
            var jsonObject = JObject.Parse(jsonContent);
            return ((string)jsonObject["result"]) == "success";
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
        }

        return false;
    }
    
    /**
        Richiesta
        {
	        "operation": "rotate_object",
	        "uid": 15248,
	        "from_rotate_x": "10",
	        "to_rotate_y": "12"
        }
        Risposta
        Caso positivo:
        {
	        "result": "success"
        }
        Caso negativo:
        {
	        "result": "failed"
        }
     */
    public bool SendObjectRotate(string uid, float fromRotateY, float toRotateY)
    {
        var requestJson = new JObject();
        requestJson["operation"] = "rotate_object";
        requestJson["uid"] = uid;
        requestJson["from_rotate_y"] = fromRotateY;
        requestJson["to_rotate_y"] = toRotateY;

        try
        {
            var jsonContent = Send(requestJson.ToString());
            var jsonObject = JObject.Parse(jsonContent);
            return ((string)jsonObject["result"]) == "success";
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
        }

        return false;
    }
    
    /**
        Richiesta
        {
	        "operation": "flip_object",
	        "uid": 15248
        }
        Risposta
        Caso positivo:
        {
	        "result": "success"
        }
        Caso negativo:
        {
	        "result": "failed"
        }
     */
    public bool SendObjectFlip(string uid)
    {
        var requestJson = new JObject();
        requestJson["operation"] = "flip_object";
        requestJson["uid"] = uid;

        try
        {
            var jsonContent = Send(requestJson.ToString());
            var jsonObject = JObject.Parse(jsonContent);
            return ((string)jsonObject["result"]) == "success";
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
        }

        return false;
    }
    
    /**
        Richiesta
        {
	        "operation": "object_released"
	        "uid": "254"
        }
        Risposta
        Caso positivo:
        {
	        "result": "success"
        }
        Caso negativo:
        {
	        "result": "failed"
        }
     */
    public bool SendReleaseObject(string uid)
    {
        var requestJson = new JObject();
        requestJson["operation"] = "release_object";
        requestJson["uid"] = uid;

        try
        {
            var jsonContent = Send(requestJson.ToString());
            var jsonObject = JObject.Parse(jsonContent);
            return (string)jsonObject["result"] == "success";
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
        }

        return false;
    }

    #endregion

    #region Card Methods

    /**
        Richiesta
        {
	        "operation": "draw_spawn"
        }
        Risposta
        {
            "result": "success",
	        "uid": "254",
            "card_id": "15"
        }
     */
    public KeyValuePair<string, int> SendDrawSpawnCard()
    {
        var requestJson = new JObject();
        requestJson["operation"] = "draw_spawn";

        try
        {
            var jsonContent = Send(requestJson.ToString());
            var jsonObject = JObject.Parse(jsonContent);
            return new KeyValuePair<string, int>((string)jsonObject["uid"], (int)jsonObject["card_id"]);
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
        }

        return new KeyValuePair<string, int>("", -1);
    }
    
    /**
        Richiesta
        {
	        "operation": "undo_draw_spawn"
	        "uid": "12",
	        "card_uid": 464
        }
        Risposta
        {
	        "result": "success"
        }
        Caso negativo:
        {
	        "result": "failed",
	        "reason": "La tua carta non è l'ultima ad essere stata pescata"
        }
     */
    public Tuple<bool, string> SendUndoDrawSpawnCard(string uid, string cardUid)
    {
        var requestJson = new JObject();
        requestJson["operation"] = "undo_draw_spawn";
        requestJson["uid"] = uid;
        requestJson["card_uid"] = cardUid;

        try
        {
            var jsonContent = Send(requestJson.ToString());
            var jsonObject = JObject.Parse(jsonContent);
            return new Tuple<bool, string>((string)jsonObject["result"] == "success", (string)jsonObject["reason"]);
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
        }

        return new Tuple<bool, string>(false, "Exception occurred");
    }
    
    
    /**
        Richiesta
        {
	        "operation": "draw_equip"
        }
        Risposta
        {
            "result": "success",
	        "uid": "254",
            "card_id": "15"
        }
     */
    public KeyValuePair<string, int> SendDrawEquipCard(string uid)
    {
        var requestJson = new JObject();
        requestJson["operation"] = "draw_equip";
        requestJson["uid"] = uid;

        try
        {
            var jsonContent = Send(requestJson.ToString());
            var jsonObject = JObject.Parse(jsonContent);
            return new KeyValuePair<string, int>((string)jsonObject["card_uid"], (int)jsonObject["card_id"]);
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
        }

        return new KeyValuePair<string, int>("", -1);
    }
    
    /**
        Richiesta
        {
	        "operation": "undo_draw_equip"
        }
        Risposta
        {
	        "result": "success"
        }
        Caso negativo:
        {
	        "result": "failed",
	        "reason": "La tua carta non è l'ultima ad essere stata pescata"
        }
     */
    public Tuple<bool, string> SendUndoDrawEquipCard(string uid, string cardUid)
    {
        var requestJson = new JObject();
        requestJson["operation"] = "undo_draw_equip";
        requestJson["uid"] = uid;
        requestJson["card_uid"] = cardUid;

        try
        {
            var jsonContent = Send(requestJson.ToString());
            var jsonObject = JObject.Parse(jsonContent);
            return new Tuple<bool, string>((string)jsonObject["result"] == "success", (string)jsonObject["result"]);
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
        }

        return new Tuple<bool, string>(false, "Exception occurred");
    }    
    
    /**
        Richiesta
        {
	        "operation": "send_card"
        }
        Risposta
        {
	        "result": "success"
        }
        Caso negativo:
        {
	        "result": "failed",
	        "reason": "La tua carta non è l'ultima ad essere stata pescata"
        }
     */
    public bool SendCard(string uid, int targetIndex, int cardUid)
    {
        var requestJson = new JObject();
        requestJson["operation"] = "send_card";
        requestJson["uid"] = uid;
        requestJson["target_index"] = targetIndex;
        requestJson["card_uid"] = cardUid;

        try
        {
            var jsonContent = Send(requestJson.ToString());
            var jsonObject = JObject.Parse(jsonContent);
            return (string)jsonObject["result"] == "success";
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
        }

        return false;
    }    

    #endregion
    
    #region Zombies Management Methods

    /**
         Richiesta
        {
	        "operation": "get_zombie",
	        "type": "abominio"
        }
        Risposta
        Caso positivo:
        {
            "result": "success",
	        "uid": "254"
        }
        Caso negativo:
        {
            "result": "failed"
        }
     */
    public Tuple<bool, Zombie> GetZombie(AppMain.ZombieType zombieType)
    {
        var requestJson = new JObject();
        requestJson["operation"] = "get_zombie";
        requestJson["type"] = JsonConvert.SerializeObject(zombieType, new StringEnumConverter());

        try
        {
            var jsonContent = Send(requestJson.ToString());
            var jsonObject = JObject.Parse(jsonContent);
            return new Tuple<bool, Zombie>((string)jsonObject["result"] == "success",
                new Zombie((string)jsonObject["uid"], zombieType,(float)jsonObject["x"], (float)jsonObject["z"]));
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
        }

        return new Tuple<bool, Zombie>(false, null);;
    }

    #endregion

    #endregion
    
    [MethodImpl(MethodImplOptions.Synchronized)]
    private string Send(String data)
    {
        appMain.zombicidePollingClient?.SuspendUpdates();
        
        byte[] msg = Encoding.ASCII.GetBytes(data);

        try
        {
//             Debug.Log(data);
            // Send the data through the socket.  
            int bytesSent = sender.Send(msg);
        
            // Data buffer for incoming data.  
            byte[] bytes = new byte[50*1024];
        
            // Receive the response from the remote device.  
            int bytesRec = sender.Receive(bytes);
        
            appMain.zombicidePollingClient?.ResumeUpdates(4000);

//            var receivedData = Encoding.ASCII.GetString(bytes, 0, bytesRec);
//            Debug.Log(receivedData);
//            return receivedData;
            return Encoding.ASCII.GetString(bytes, 0, bytesRec);
        }
        catch (Exception e)
        {
            if (e.GetType() == typeof(SocketException))
            {
                if (connected)
                {
                    HFTDialog.MessageBox("Attenzione", "(error:0001)\n\nIl server ha terminato la connessione", "OK", () =>
                    {
                        StopClient();
                        appMain.zombicidePollingClient.StopClient();
                        Application.Quit();
                    });
                }
                else
                {
                    HFTDialog.MessageBox("Attenzione", "(error:0002)\n\nImpossibile stabilire una connessione con il server", "OK", Application.Quit);
                }
            }
        }

        return null;
    }

    
}