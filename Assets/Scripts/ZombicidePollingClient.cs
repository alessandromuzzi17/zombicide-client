using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UnityEngine;

public class ZombicidePollingClient
{
    public bool updatesSuspended;

    private AppMain appMain;
    private bool connected;
    private Socket sender;
    private IPAddress remoteIpAddress;
    private Timer pollingTimer;
    private Timer resumeUpdatesTimer;
    private double POLLING_TIME_FREQUENCY = 1; // in seconds
    private Action _pollingCallback;
    
    public ZombicidePollingClient(AppMain _appMain, Action pollingCallback, IPAddress ipAddress = null)
    {
        appMain = _appMain;
        connected = false;
        _pollingCallback = pollingCallback;
        updatesSuspended = false;
        if (ipAddress == null)
        {
            // Establish the remote endpoint for the socket.  
            // This example uses port 11000 on the local computer.  
            IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());
            
            remoteIpAddress = ipHostInfo.AddressList.First(it => it.AddressFamily == AddressFamily.InterNetwork);
        }
        resumeUpdatesTimer = new Timer(_ => updatesSuspended = false);
        StartClient();
    }
    
    public void StartClient()
    {
        // Connect to a remote device.  
        try
        {
            // Create a TCP/IP  socket.  
            // var remoteHostEntry = Dns.GetHostEntry(Dns.GetHostName());
            var remoteHostEntry = Dns.GetHostEntry("casamuzzi.ddns.net");
            var remoteIpAddr = remoteHostEntry.AddressList[0];
            var remoteEndPoint = new IPEndPoint(remoteIpAddr, 11001);
            sender = new Socket(remoteIpAddr.AddressFamily, SocketType.Dgram, ProtocolType.Udp);

            // Connect the socket to the remote endpoint. Catch any errors.  
            try
            {
                sender.Connect(remoteEndPoint);
                connected = true;
                
                pollingTimer = new Timer((e) => _pollingCallback(), 
                    null, 
                    TimeSpan.Zero, 
                    TimeSpan.FromSeconds(POLLING_TIME_FREQUENCY));
            }
            catch (ArgumentNullException ane)
            {
                Debug.Log($"ArgumentNullException : {ane}");
            }
            catch (SocketException se)
            {
                Debug.Log($"SocketException : {se}");
            }
            catch (Exception e)
            {
                Debug.Log($"Unexpected exception : {e}");
            }

        }
        catch (Exception e)
        {
            Debug.Log(e.ToString());
        }
    }

    public void StopClient()
    {
        pollingTimer?.Change(Timeout.Infinite, Timeout.Infinite);

        if (sender != null)
        {
            // Release the socket.  
            sender.Shutdown(SocketShutdown.Both);
            sender.Close();
        }
    }

    public void SuspendUpdates()
    {
        updatesSuspended = true;
    }
    
    public void ResumeUpdates(int offsetMillisBeforeResume = 0)
    {
        restartUpdatesTimer(offsetMillisBeforeResume);
    }
    
    /**
        Richiesta
        {
	        "operation": "get_pawns"
        }
        Risposta
      //  Caso positivo:
      //  {
	  //      "result": "success"
      //  }
      //  Caso negativo:
      //  {
	  //      "result": "failed"
      //  }
     */
    public UpdateFromServer SendGetUpdates(string uid)
    {
        // if (updatesSuspended)
        //     return null;
        
        var requestJson = new JObject
        {
            ["operation"] = "get_updates",
            ["uid"] = uid
        };

        try
        {
            var jsonContent = Send(requestJson.ToString());
            Debug.Log($"polling - {updatesSuspended} - {jsonContent}");
            var jsonObject = JObject.Parse(jsonContent);

            if (updatesSuspended || ((string)jsonObject["result"]) == "failed")
                return null;
            
            return new UpdateFromServer(
                JsonConvert.DeserializeObject<Dictionary<string, Pawn>>((string) jsonObject["pawns"]),
                JsonConvert.DeserializeObject<Dictionary<string, Player>>((string) jsonObject["players"]),
                JsonConvert.DeserializeObject<Dictionary<string, Zombie>>((string) jsonObject["zombies"]),
                JsonConvert.DeserializeObject<Dictionary<string, int>>((string) jsonObject["cards"]));
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
        }

        return null;
    }

    private string Send(String data)
    {
        // sendMethodMutex.WaitOne();
        
        byte[] msg = Encoding.ASCII.GetBytes(data);
        
        try
        {
            // Send the data through the socket.  
            int bytesSent = sender.Send(msg);
        
            // Data buffer for incoming data.  
            byte[] bytes = new byte[50*1024];
        
            // Receive the response from the remote device.  
            int bytesRec = sender.Receive(bytes);

            return Encoding.ASCII.GetString(bytes, 0, bytesRec);
        }
        catch (Exception e)
        {
            if (e.GetType() == typeof(SocketException))
            {
                if (connected)
                {
                    HFTDialog.MessageBox("Attenzione", "(error:0003)\n\nIl server ha terminato la connessione", "OK", () =>
                    {
                        StopClient();
                        appMain.zombicideClient.StopClient();
                        Application.Quit();
                    });
                }
                else
                {
                    HFTDialog.MessageBox("Attenzione", "(error:0004)\n\nImpossibile stabilire una connessione con il server", "OK", Application.Quit);
                }
            }
        }

        return null;
    }

    private void restartUpdatesTimer(int millis)
    {
        resumeUpdatesTimer.Change(millis, Timeout.Infinite);
    }
}