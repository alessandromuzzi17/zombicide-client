using UnityEngine;

public class DragZAxis : MonoBehaviour
{
    public float minZ;
    public float maxZ;
    public float speed;

    private Vector3 distance;
    private Camera cam;

    private void Start()
    {
        cam = Camera.main;
    }

    void OnMouseDown()
    {
        distance = cam.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, cam.WorldToScreenPoint(transform.position).z)) - transform.position;
    }
  
    void OnMouseDrag()
    {
        Vector3 distance_to_screen = cam.WorldToScreenPoint(transform.position);
        Vector3 pos_move = cam.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, distance_to_screen.z ));
        var newZ = pos_move.z - distance.z*speed;
        if (newZ < minZ)
            newZ = minZ;
        if (newZ > maxZ)
            newZ = maxZ;
        transform.position = new Vector3( transform.position.x , transform.position.y,  newZ);
    }
}