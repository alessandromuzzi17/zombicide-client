﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Image = UnityEngine.UIElements.Image;

public class PlayerOverviewBehaviour : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    private AppMain appMain;
    
    private void Start()
    {
        appMain = AppMain.instance;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        var requestedIndex = Int32.Parse(gameObject.GetComponent<TextMeshProUGUI>().text[1].ToString());
        var requestedPlayer = appMain.connectedPlayers.FirstOrDefault(it => it.Value.Index == requestedIndex).Value;
        if (requestedPlayer != null)
        {
            var playerEquipCards = requestedPlayer.EquipCards.Values.ToList();
            var overviewTransform = appMain.playerOverviewInstance.transform;
            // populate cards overview with requested player's cards
            // void cards before and after non-void cards
            int playerCardsCount = Math.Min(playerEquipCards.Count, 5);
            int voidLateralCards = (5 - playerCardsCount) / 2;
            float voidInnerCard = (5 - playerCardsCount) % 2f;
            int nonVoidCards = (int)(5 - voidLateralCards*2 - voidInnerCard);
            var firstCard = overviewTransform.GetChild(0);
            var emptyCardMaterial = new Material(firstCard.GetComponent<UnityEngine.UI.Image>().material)
            {
                mainTexture = appMain.emptyCard
            };
            // populate void cards before
            for (int i = 0; i < voidLateralCards; i++)
            {
                overviewTransform.GetChild(i).GetComponent<UnityEngine.UI.Image>().material = emptyCardMaterial;
            }
            // populate non-void cards
            for (int i = voidLateralCards; i < voidLateralCards + nonVoidCards; i++)
            {
                var currentCard = overviewTransform.GetChild(i);
                var currentCardMaterial = new Material(firstCard.GetComponent<UnityEngine.UI.Image>().material)
                {
                    mainTexture = appMain.equipCards[playerEquipCards[i-voidLateralCards]]
                };
                currentCard.GetComponent<UnityEngine.UI.Image>().material = currentCardMaterial;
            }
            // populate void cards after
            for (int i = voidLateralCards + nonVoidCards; i < 5; i++)
            {
                overviewTransform.GetChild(i).GetComponent<UnityEngine.UI.Image>().material = emptyCardMaterial;
            }

            appMain.playerOverviewInstance.SetActive(true);
        }
    }
 
    public void OnPointerExit(PointerEventData eventData)
    {
        appMain.playerOverviewInstance.SetActive(false);
    }
}
