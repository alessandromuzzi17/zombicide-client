﻿using UnityEngine;

public class BtnMinimize : MonoBehaviour
{
    public void OnClick()
    {
        Screen.fullScreen = !Screen.fullScreen;
    }
}