﻿using UnityEngine;

public class BtnUndoSearch : MonoBehaviour
{
    #region Public Fields

    public AppMain appMain;

    #endregion

    public void OnClick()
    {
        if (appMain.usedEquipCards.Count == 0)
        {
            HFTDialog.MessageBox("Error", "Impossibile annullare la pescata", "OK", () => {});
            return;
        }
        
        // ask the server to undo draw equip card
        var result = appMain.zombicideClient.SendUndoDrawEquipCard(appMain.currentPlayer.Uid, appMain.usedEquipCards.Peek().Key);
        if (result.Item1)
        {
            appMain.UndoEquipCard();
        }
        else
        {
            HFTDialog.MessageBox("Error", result.Item2, "OK", () => {});
        }
    }
}
