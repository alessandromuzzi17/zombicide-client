﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class BtnEndRound : MonoBehaviour
{
    #region Public Fields

    public AppMain appMain;

    #endregion

    public void OnClick()
    {
        appMain.EndRound();
    }
}
