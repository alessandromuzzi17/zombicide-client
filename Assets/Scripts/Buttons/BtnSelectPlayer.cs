﻿using UnityEngine;
using UnityEngine.UI;
using Image = UnityEngine.UI.Image;

public class BtnSelectPlayer : MonoBehaviour
{
    private AppMain appMain;

    private void Start()
    {
        appMain = AppMain.instance;
    }

    public void OnClick()
    {
        appMain.choicePanel.SetActive(false);
        HFTDialog.MessageInputBox("Ciao giocatore", "Come ti chiami?", "Ok",
            userName =>
            {
                var newPlayer = new Player(gameObject.tag, userName, Utils.Utils.GetRandomPosInDefaultSpace(), 0f, 0f);
                var result = appMain.zombicideClient.SendNewPlayer(newPlayer);
                if (result.Key)
                {
                    appMain.txtPlayerNumber.text = "#" + result.Value.Index;
                    appMain.playerTile.GetComponent<MeshRenderer>().material.mainTexture = GetComponent<Image>().sprite.texture;
                    // we need to create the current player here in order to start the polling server
                    appMain.CreateObject(result.Value.Uid, result.Value);
                }
                else
                {
                    HFTDialog.MessageBox("Error", "Giocatore non disponibile", "OK", () => {});
                    gameObject.GetComponent<Button>().interactable = false;
                    appMain.choicePanel.SetActive(true);
                }
            }, "Annulla", text => appMain.choicePanel.SetActive(true) );
    }
}
