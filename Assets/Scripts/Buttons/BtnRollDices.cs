﻿using System;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class BtnRollDices : MonoBehaviour
{
    public GameObject dicesBox;
    public GameObject dice;

    private Button button;
    private long lastEscPress;
    private const long MIN_ESC_PRESS_DELTA = 2500000;
    private List<GameObject> activeDices;
    private bool dicesBoxCanDisappear;
    private Thread thdDiceRoll;

    private void Start()
    {
        button = GetComponent<Button>();
        activeDices = new List<GameObject>();
    }

    public void OnClick()
    {
        button.interactable = false;
        
        HFTDialog.MessageInputBox("Quanti dadi vuoi tirare?", "Immetti il numero di dadi da tirare:", "Ok", 
            text =>
            {
                var dicesNumber = 0;
                var parseResult = Int32.TryParse(text, out dicesNumber);
                if (parseResult && dicesNumber > 0 && dicesNumber <= 6 && activeDices.Count == 0)
                {
                    dicesBox.SetActive(true);
                    dicesBoxCanDisappear = false;
                    
                    thdDiceRoll = new Thread(() =>
                    {
                        var yPosOffset = (43 - 28) / dicesNumber;
                        for (int i = 0; i < dicesNumber; i++)
                        {
                            // accessing UI Thread in order to create the dice
                            UnityMainThreadDispatcher.Instance().Enqueue(() =>
                            {
                                var newDice = Instantiate(dice, Vector3.zero, Quaternion.identity);
                                float dirX = Random.Range (0, 500);
                                float dirY = Random.Range (0, 500);
                                float dirZ = Random.Range (0, 500);
                                var diceTransform = newDice.transform;
                                var diceXZPos = Utils.Utils.GetRandomXZPos(-15, 15,-8, -23);
                                var dicePos = new Vector3(diceXZPos.x,  28 + yPosOffset * i , diceXZPos.y);
                                diceTransform.rotation = Quaternion.identity;
                                diceTransform.position = dicePos;
                                var rb = newDice.transform.GetChild(0).GetComponent<Rigidbody>();
                                rb.AddForce (diceTransform.up * 200);
                                rb.AddTorque (dirX, dirY, dirZ);
                                activeDices.Add(newDice);
                            });
                            
                            // wait to ensure the pos generation is random
                            Thread.Sleep(300);
                        }
                    });
                    
                    thdDiceRoll.Start();
                    
                    dicesBoxCanDisappear = true;
                }
                else
                {
                    HFTDialog.MessageBox("Attenzione", "Valore immesso non valido", "Ok",() => button.interactable = true );
                }
            },"Annulla", text => button.interactable = true);
    }

    private void Update()
    {
        if(dicesBoxCanDisappear && Input.GetKey(KeyCode.Escape) && DateTime.Now.Ticks - lastEscPress > MIN_ESC_PRESS_DELTA)
        {
            activeDices.ForEach(Destroy);
            activeDices.Clear();
            dicesBox.SetActive(false);
            
            lastEscPress = DateTime.Now.Ticks;
            button.interactable = true;
        }
    }
}