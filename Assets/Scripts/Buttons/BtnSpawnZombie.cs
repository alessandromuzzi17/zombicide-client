﻿using UnityEditor;
using UnityEngine;

public class BtnSpawnZombie : MonoBehaviour
{
    #region Public Fields

    public AppMain appMain;
    public AppMain.ZombieType zombieType;
    public GameObject zombie;

    #endregion

    public void OnClick()
    {
        var result = appMain.zombicideClient.GetZombie(zombieType);
        if (result.Item1)
        {
            var zombiePos = new Vector3(result.Item2.CoordX, Utils.Utils.GetRandomYInDefaultSpace(), result.Item2.CoordZ);
            var zombieGameObject = Instantiate(zombie, zombiePos, Quaternion.identity);
            zombieGameObject.name = result.Item2.Uid;
            appMain.AddObject(result.Item2.Uid, zombieGameObject);
        }
        else
        {
            HFTDialog.MessageBox("Attenzione", "Non sono più disponibili zombie del tipo richiesti", "OK", () => {});
        }
    }
}
