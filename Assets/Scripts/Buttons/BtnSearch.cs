﻿using UnityEngine;
using UnityEngine.UI;

public class BtnSearch : MonoBehaviour
{
    #region Public Fields

    public AppMain appMain;
    public GameObject card;

    #endregion

    public void OnClick()
    {
        var result = appMain.zombicideClient.SendDrawEquipCard(appMain.currentPlayer.Uid);
        appMain.currentPlayer.EquipCards.Add(result.Key, result.Value);
        var newEquipCard = Instantiate(card, new Vector3(-2f, 25f, -16f), Quaternion.identity);
        newEquipCard.name = result.Key;
        newEquipCard.GetComponent<Renderer>().material.mainTexture = appMain.equipCards[result.Value];
        newEquipCard.GetComponent<Renderer>().material.mainTextureScale = new Vector2(-1, -1);
        appMain.usedEquipCards.Push(result);
        appMain.AddObjectWithoutPawn(result.Key, newEquipCard);
    }
}
