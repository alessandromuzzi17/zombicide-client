﻿using UnityEngine;

public class BtnWound : MonoBehaviour
{
    #region Public Fields

    public AppMain appMain;
    public GameObject card;

    #endregion

    public void OnClick()
    {
        var newSpawnCard = Instantiate(card, new Vector3(-2f, 25f, -16f), Quaternion.identity);
        newSpawnCard.name = "wound" + AppMain.IdGenerator.GetNewIDString();
        newSpawnCard.tag = "card_wound";
        newSpawnCard.GetComponent<Renderer>().material.mainTexture = appMain.equipCards[AppMain.WOUND_CARD_CODE];
        newSpawnCard.GetComponent<Renderer>().material.mainTextureScale = new Vector2(-1, -1);
        appMain.AddObjectWithoutPawn(newSpawnCard.name, newSpawnCard);
    }
}
