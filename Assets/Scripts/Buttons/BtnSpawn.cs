﻿using UnityEngine;
using UnityEngine.UI;

public class BtnSpawn : MonoBehaviour
{
    #region Public Fields

    public AppMain appMain;
    public GameObject card;

    #endregion

    public void OnClick()
    {
        var result = appMain.zombicideClient.SendDrawSpawnCard();
        var newSpawnCard = Instantiate(card, new Vector3(-2f, 25f, -16f), Quaternion.identity);
        newSpawnCard.name = result.Key.ToString();
        newSpawnCard.GetComponent<Renderer>().material.mainTexture = appMain.spawnCards[result.Value];
        newSpawnCard.GetComponent<Renderer>().material.mainTextureScale = new Vector2(-1, -1);
        appMain.usedSpawnCards.Push(result);
        appMain.AddObjectWithoutPawn(result.Key, newSpawnCard);
    }
}
