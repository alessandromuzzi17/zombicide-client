﻿using UnityEngine;

public class BtnUndoSpawn : MonoBehaviour
{
    #region Public Fields

    public AppMain appMain;

    #endregion

    public void OnClick()
    {
        if (appMain.usedSpawnCards.Count == 0)
        {
            HFTDialog.MessageBox("Error", "Impossibile annullare la pescata", "OK", () => {});
            return;
        }
        
        // ask the server to undo draw spawn card
        var result = appMain.zombicideClient.SendUndoDrawSpawnCard(appMain.currentPlayer.Uid, appMain.usedSpawnCards.Peek().Key);
        if (result.Item1)
        {
            appMain.UndoSpawnCard();
        }
        else
        {
            HFTDialog.MessageBox("Error", result.Item2, "OK", () => {});
        }
    }
}
