using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using TMPro;
using UnityEngine;
using Utils;
using Utils.ZombicideServer.Utils;

public class AppMain : MonoBehaviour
{
    #region Consts

    [NonSerialized]
    public static readonly int WOUND_CARD_CODE = 28;

    #endregion
    
    #region Public Fields

    // Serialized
    public float minFov = 15f;
    public float maxFov = 90f;
    public float sensitivity = 10f;

    public Texture[] spawnCards;
    public Texture[] equipCards;
    public Texture[] tiles;
    public Texture emptyCard;
    public GameObject[] zombies;
    public GameObject[] playerObjects;
    public GameObject mainCanvas;
    public GameObject choicePanel;
    public GameObject playerTile;
    public GameObject currentPlayerDish;
    public GameObject btnEndRound;
    public GameObject cardObj;
    public TextMeshProUGUI txtPlayerNumber;
    public GameObject malePlayer;
    public GameObject femalePlayer;
    public GameObject playerSummaryLabel;
    public GameObject playerOverview;
    [NonSerialized]
    public GameObject playerOverviewInstance;

    public Stack<KeyValuePair<string, int>> usedSpawnCards { get; } = new Stack<KeyValuePair<string, int>>();
    public Stack<KeyValuePair<string, int>> usedEquipCards { get; } = new Stack<KeyValuePair<string, int>>();
    
    public static AppMain instance { get; private set; }
    public static IdGenerator IdGenerator;
    [NonSerialized]
    public bool isDebug;
    
    public ZombicideClient zombicideClient { get; private set; }
    public ZombicidePollingClient zombicidePollingClient { get; private set; }
    
    [NonSerialized]
    public Dictionary<string, Player> connectedPlayers = new Dictionary<string, Player>();
    [NonSerialized]
    public Player currentPlayer;

    #endregion

    #region Private Fields

    private readonly Dictionary<string, Color> MALE_PLAYERS = new Dictionary<string, Color>
    {
        {"Dan", Colors.OrangeCrayola},
        {"Travis", Colors.RedCrayola},
        {"Joe", Colors.BlueCrayola},
        {"Bear", Colors.GrannySmithApple},
        {"James", Colors.SizzlingRed}
    };
    private readonly Dictionary<string, Color> FEMALE_PLAYERS = new Dictionary<string, Color>
    {
        {"Laurie", Colors.GreenCrayola},
        {"Maddie", Colors.YellowCrayola},
        {"Parker", Colors.SkyBlueCrayola},
        {"Jane", Colors.CobaltBlue},
        {"Louise", Colors.VividViolet}
    };

    private long lastKeyPress;
    private const long MIN_KEY_PRESS_DELTA = 2500000;
    
    private Tuple<System.Drawing.Size, string[]> table;
    
    private Dictionary<string, GameObject> activeObjects = new Dictionary<string, GameObject>();
    private Dictionary<string, Pawn> activePawns = new Dictionary<string, Pawn>();
    
    // runtime created instance
    private GameObject currentPlayerDishInstance;

    private Camera mainCamera;

    #endregion

    [JsonConverter(typeof(StringEnumConverter))] 
    public enum ZombieType
    {
        Abominio,
        Deambulante,
        Grassone,
        Corridore,
        Strisciante
    }

    public AppMain()
    {
        instance = this;
        IdGenerator = new IdGenerator();
        isDebug = true;
    }

    private void Awake()
    {
        #region Client - Server

        if(zombicideClient == null)
        {
            zombicideClient = new ZombicideClient(this);
        }
        table = zombicideClient.GetTable();

        SetupGamePlane();
        playerOverviewInstance = Instantiate(playerOverview, Vector3.zero, Quaternion.identity);
        playerOverviewInstance.transform.SetParent(mainCanvas.transform);
        var playerOverviewRectTransform = playerOverviewInstance.GetComponent<RectTransform>();
        playerOverviewRectTransform.anchorMin = new Vector2(0.5f, 0.5f);
        playerOverviewRectTransform.anchorMax = new Vector2(0.5f, 0.5f);
        playerOverviewRectTransform.anchoredPosition = Vector2.zero;

        #endregion
        
        mainCamera = Camera.main;
    }

    private void SetupGamePlane()
    {
        var gamePlane = GameObject.FindWithTag("game_plane");
        int i = 0;
        foreach(Transform tile in gamePlane.transform)
        {
            var tileCode = tile.name.Split('#')[1];
            var tileCoords = new Tuple<int, int>(tileCode[0] - '0', tileCode[1] - '0');
            if (tileCoords.Item1 >= table.Item1.Height || tileCoords.Item2 >= table.Item1.Width)
            {
                 tile.gameObject.SetActive(false);
            }
            else
            {
                var currentTIleDescription = table.Item2[i];
                var currentTileIdx = currentTIleDescription[0] - '0' - 1 + ((currentTIleDescription[1] == 'V')? 9 : 0);
                tile.GetComponent<MeshRenderer>().material.mainTexture = tiles[currentTileIdx];
        
                switch (currentTIleDescription.Split()[1])
                {
                    case "up":
                        break;
                    
                    case "down":
                        tile.transform.Rotate(0f, 180f, 0f);
                        break;
                    
                    case "left":
                        tile.transform.Rotate(0f, 90f, 0f);
                        break;
                    
                    case "right":
                        tile.transform.Rotate(0f, -90f, 0f);
                        break;
                }
                
                i++;
            }
        }
        
        // enlarge the game plane
        var actualScale = gamePlane.transform.localScale;
        float xScaleFactor = 4.0f / table.Item1.Width;
        float zScaleFactor = 4.0f / table.Item1.Height;
        var scaleFactor = Math.Min(xScaleFactor, zScaleFactor);
        gamePlane.transform.localScale = new Vector3(actualScale.x * scaleFactor, actualScale.y, actualScale.z * scaleFactor);
        gamePlane.transform.rotation = Quaternion.Euler(0f, 180f, 0f);

        #region Scaled Game Plane Positioning

        float newPosX = 0;
        float newPosZ = 0;
        
        switch (table.Item1.Height)
        {
            case 1:
                switch (table.Item1.Width)
                {
                    case 1:
                        newPosX = 43.4f;
                        newPosZ = -236f;
                        break;
            
                    case 2:
                        newPosX = 12f;
                        newPosZ = -125.1f;
                        break;
            
                    case 3:
                        newPosX = 1f;
                        newPosZ = -88f;
                        break;
                    
                    case 4:
                        newPosX = -4.2f;
                        newPosZ = -69.34f;
                        break;
                }
                break;
            
            case 2:
                switch (table.Item1.Width)
                {
                    case 1:
                        newPosX = 22.7f;
                        newPosZ = -103.8f;
                        break;
            
                    case 2:
                        newPosX = 12f;
                        newPosZ = -103.8f;
                        break;
            
                    case 3:
                        newPosX = 1f;
                        newPosZ = -74f;
                        break;
                    
                    case 4:
                        newPosX = -4.2f;
                        newPosZ = -59f;
                        break;
                }
                break;
            
            case 3:
                switch (table.Item1.Width)
                {
                    case 1:
                        newPosX = 16f;
                        newPosZ = -60f;
                        break;
            
                    case 2:
                        newPosX = 9f;
                        newPosZ = -60.4f;
                        break;
            
                    case 3:
                        newPosX = 1f;
                        newPosZ = -60f;
                        break;
                    
                    case 4:
                        newPosX = -4.2f;
                        newPosZ = -48.6f;
                        break;
                }
                break;
            
            case 4:
                switch (table.Item1.Width)
                {
                    case 1:
                        newPosX = 11.6f;
                        newPosZ = -38;
                        break;
            
                    case 2:
                        newPosX = 6.5f;
                        newPosZ = -38f;
                        break;
            
                    case 3:
                        newPosX = 1.34f;
                        newPosZ = -38f;
                        break;
                    
                    case 4:
                        newPosX = 1.4f;
                        newPosZ = -37.8f;
                        break;
                }
                break;
        }

        #endregion

        gamePlane.transform.position = new Vector3(newPosX,16.446f, newPosZ);
    }

    private void Update()
    {
        if (DateTime.Now.Ticks - lastKeyPress > MIN_KEY_PRESS_DELTA
            && (Input.GetKey(KeyCode.Plus)
                || Input.GetKey(KeyCode.Minus)
                || Input.GetKey(KeyCode.KeypadPlus)
                || Input.GetKey(KeyCode.KeypadMinus)))
        {
            // fov movement
            if (mainCamera == default)
            {
                mainCamera = Camera.main;
            }
            float fov  = mainCamera.fieldOfView;
            if (Input.GetKey(KeyCode.Plus) || Input.GetKey(KeyCode.KeypadPlus))
            {
                fov -= sensitivity;
            }
            else if (Input.GetKey(KeyCode.Minus) || Input.GetKey(KeyCode.KeypadMinus))
            {
                fov += sensitivity;
            }
            fov = Mathf.Clamp(fov, minFov, maxFov);
            mainCamera.fieldOfView = fov;

            lastKeyPress = DateTime.Now.Ticks;
        }
    }

    #region Objects Management Methods

    #region Do Methods

    public void CreateObject(string uid, Pawn pawn)
    {
        if (!activePawns.ContainsKey(uid))
        {
            UnityMainThreadDispatcher.Instance().Enqueue(() =>
            {
                #region Recognizing Pawn Type

                GameObject pawnGameObj;
                if (pawn is Zombie zombiePawn)
                {
                    int zombieTypeIdx;
                    switch (zombiePawn.Type)
                    {
                        case ZombieType.Abominio:
                            zombieTypeIdx = 0;
                            break;

                        case ZombieType.Deambulante:
                            zombieTypeIdx = 1;
                            break;
                        
                        case ZombieType.Grassone:
                            zombieTypeIdx = 2;
                            break;
                        
                        case ZombieType.Corridore:
                            zombieTypeIdx = 3;
                            break;
                        
                        case ZombieType.Strisciante:
                            zombieTypeIdx = 4;
                            break;
                        
                        default:
                            zombieTypeIdx = 1;
                            break;
                    }

                    pawnGameObj = zombies[zombieTypeIdx];
                }
                else if (pawn is Player playerPawn)
                {
                    connectedPlayers.Add(playerPawn.Uid, playerPawn);
                    
                    // is a male
                    if (MALE_PLAYERS.ContainsKey(playerPawn.Name))
                    {
                        pawnGameObj = malePlayer;
                    }
                    //is a female
                    else
                    {
                        pawnGameObj = femalePlayer;
                    }

                    pawnGameObj.transform.GetChild(1).gameObject.GetComponent<TextMeshPro>().text = "#" + playerPawn.Index + " " + playerPawn.Name;

                    if (currentPlayer == null)
                    {
                        currentPlayer = playerPawn;
                        activePawns.Add(uid, pawn);
                    }

                    if (zombicidePollingClient == null)
                    {
                        zombicidePollingClient = new ZombicidePollingClient(this, UpdateGame);
                    }
                }
                else
                {
                    pawnGameObj = playerObjects.FirstOrDefault(it => it.name == pawn.Uid);
                    if (pawnGameObj != null && !Int32.TryParse(pawn.Uid, out int _) && !pawn.Uid.Contains("Door"))
                    {
                        pawnGameObj.GetComponent<Renderer>().material.mainTextureScale = new Vector2(-1, -1);
                    }
                }
                
                #endregion

                #region Creating the object

                if (pawnGameObj != null)
                {
                    var newObj = Instantiate(pawnGameObj,
                        new Vector3(pawn.CoordX, Utils.Utils.GetRandomYInDefaultSpace(), pawn.CoordZ),
                        Quaternion.Euler(pawn.FlipX, pawn.RotationY, 0f));
                    newObj.name = uid;
                    pawn.CoordY = newObj.transform.position.y;

                    if (pawn is Player playerPawn)
                    {
                        Transform pawnShape;
                        // is a male
                        if (MALE_PLAYERS.ContainsKey(playerPawn.Name))
                        {
                            pawnShape = newObj.transform.GetChild(0).transform.GetChild(0);
                            var newMaterial = new Material(pawnShape.GetComponent<MeshRenderer>().sharedMaterial)
                            {
                                color = MALE_PLAYERS[playerPawn.Name]
                            };
                            pawnShape.GetComponent<MeshRenderer>().sharedMaterial = newMaterial;
                        }
                        //is a female
                        else
                        {
                            pawnShape = newObj.transform.GetChild(0).transform.GetChild(0);
                            var newMaterial = new Material(pawnShape.GetComponent<MeshRenderer>().sharedMaterial)
                            {
                                color = FEMALE_PLAYERS[playerPawn.Name]
                            };
                            pawnShape.GetComponent<MeshRenderer>().sharedMaterial = newMaterial;
                        }

                        var playerLabelText = "#" + playerPawn.Index + " " + playerPawn.Name;
                        newObj.transform.GetChild(1).gameObject.GetComponent<TextMeshPro>().text = playerLabelText;
                        
                        // adding the player label
                        var playerLabel = Instantiate(this.playerSummaryLabel,
                            Vector3.zero,
                            Quaternion.identity);
                        playerLabel.transform.SetParent(mainCanvas.transform);
                        // if the index is even then anchor the label to the left, otherwise to the right
                        var isPlayerIdxEven = playerPawn.Index % 2 == 0;
                        var playerLabelRectTransform = playerLabel.GetComponent<RectTransform>();
                        playerLabelRectTransform.anchorMin = new Vector2(isPlayerIdxEven? 0 : 1 ,1);
                        playerLabelRectTransform.anchorMax = new Vector2(isPlayerIdxEven? 0 : 1 ,1);
                        playerLabelRectTransform.anchoredPosition = new Vector3(isPlayerIdxEven? 107 : -26, -76 - 50 * (playerPawn.Index / 2), 0);
                        playerLabel.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = playerLabelText;
                        playerLabel.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = playerPawn.Alias;
                    }
                    
                    AddObject(uid, newObj, pawn);
                }

                #endregion
            });
        }
    }

    public bool AddObjectWithoutPawn(string uid, GameObject newGameObject)
    {
        if (!activeObjects.ContainsKey(uid))
        {
            activeObjects.Add(uid, newGameObject);
            return true;
        }

        return false;
    }
    
    public bool AddObject(string uid, GameObject newGameObject, Pawn pawn = null)
    {
        if (!activeObjects.ContainsKey(uid))
        {
            activeObjects.Add(uid, newGameObject);
            if (pawn == null)
            {
                pawn = new Pawn(newGameObject);
            }

            if (!activePawns.ContainsKey(uid))
            {
                activePawns.Add(uid, pawn);
            }
            
            return true;
        }

        return false;
    }
    
    public bool MoveObject(string uid, float x, float z)
    {
        if (activeObjects.ContainsKey(uid))
        {
            UnityMainThreadDispatcher.Instance().Enqueue(() =>
            {
                if (Math.Abs(activeObjects[uid].transform.position.x - x) > 0.1 || Math.Abs(activeObjects[uid].transform.position.z - z) > 0.1)
                {
                    activeObjects[uid].transform.position = new Vector3(x, activeObjects[uid].transform.position.y, z);
                    activePawns[uid].UpdateWithGameObject(gameObject);
                }
            });
            
            return true;
        }

        return false;
    }
    
    public bool RotateObject(string uid, float y)
    {
        if (activeObjects.ContainsKey(uid))
        {
            UnityMainThreadDispatcher.Instance().Enqueue(() =>
            {
                if (Math.Abs(activeObjects[uid].transform.rotation.eulerAngles.y - y) > 2)
                {
                    var objectTransform = activeObjects[uid].transform;
                    objectTransform.rotation = Quaternion.Euler(objectTransform.rotation.x, y, objectTransform.rotation.z);
                    activePawns[uid].UpdateWithGameObject(gameObject);
                }
            });

            return true;
        }

        return false;
    }
    
    public bool FlipObject(string uid, float x)
    {
        if (activeObjects.ContainsKey(uid))
        {
            UnityMainThreadDispatcher.Instance().Enqueue(() =>
            {
                if (Math.Abs(activeObjects[uid].transform.rotation.eulerAngles.x - x) > 2)
                {
                    var objectTransform = activeObjects[uid].transform;
                    objectTransform.rotation = Quaternion.Euler(x, objectTransform.rotation.y, objectTransform.rotation.z);
                    activePawns[uid].UpdateWithGameObject(gameObject);
                }
            });
            

            return true;
        }

        return false;
    }

    public bool ReleaseObject(string uid)
    {
        if(activeObjects.ContainsKey(uid))
        {
            if (currentPlayer.EquipCards.ContainsKey(uid))
            {
                currentPlayer.EquipCards.Remove(uid);
            }
            activeObjects.Remove(uid);
            activePawns.Remove(uid);
            return true;
        }

        return false;
    }

    #endregion

    #endregion
    
    #region Cards Management Methods
   
    // public Texture GetEquipCard(int instanceId)
    // {
    //     var newCard = equipDeck.Pop();
    //     usedEquipCards.Push(new KeyValuePair<int, Texture>(instanceId, newCard));
    //     return newCard;
    // }
    //
    // public KeyValuePair<bool, KeyValuePair<int, Texture>> UndoEquipCard()
    // {
    //     var usedCard = usedEquipCards.Peek();
    //     GameObject cardToDestroy = GameObject.Find(usedCard.Key.ToString());
    //     var result = true;
    //     if (cardToDestroy != null)
    //     {
    //         usedEquipCards.Pop();
    //         equipDeck.Push(usedCard.Value);
    //         Destroy(cardToDestroy);
    //     }
    //     else
    //     {
    //         HFTDialog.MessageBox("Errore", "Impossible annullare l'operazione, la carta è stata rimossa.", "OK", () => {});
    //         result = false;
    //     }
    //
    //     return new KeyValuePair<bool, KeyValuePair<int, Texture>>(result, usedCard);;
    // }
    //
    // public Texture GetSpawnCard(int instanceId)
    // {
    //     var newCard = spawnDeck.Pop();
    //     usedSpawnCards.Push(new KeyValuePair<int, Texture>(instanceId, newCard));
    //     return newCard;
    // }
    //
    public void UndoSpawnCard()
    {
        var usedCard = usedSpawnCards.Pop();

        if (activeObjects.ContainsKey(usedCard.Key))
        {
            Destroy(activeObjects[usedCard.Key]);
        }
    }
    
    public void UndoEquipCard()
    {
        var usedCard = usedEquipCards.Pop();

        if (activeObjects.ContainsKey(usedCard.Key))
        {
            Destroy(activeObjects[usedCard.Key]);
        }
    }
    
    #endregion

    #region Round Management Methods

    public void StartRound()
    {
        currentPlayerDishInstance = Instantiate(currentPlayerDish, new Vector3(-17.55f, 23,-17.7f), Quaternion.identity);
        btnEndRound.SetActive(true);
    }
    
    public void EndRound()
    {
        if (currentPlayerDishInstance != null)
        {
            Destroy(currentPlayerDishInstance);
            btnEndRound.SetActive(false);
        }
    }

    #endregion
    
    #region Process Received Data Methods

    private void ProcessReceivedData(TcpClient tcpClient, string jsonContent)
    {
        var jsonObject = JObject.Parse(jsonContent);
        switch ((string) jsonObject["operation"])
        {
            case "move_object":
                PerformMoveObject(jsonObject);
                break;
            
            case "rotate_object":
                PerformRotateObject(jsonObject);
                break;
            
            case "flip_object":
                PerformFlipObject(jsonObject);
                break;
            
            case "release_object":
                PerformReleaseObject(jsonObject);
                break;
        }
    }
    
    #region Objects Management Methods

    private void PerformMoveObject(JObject jsonObject)
    {
        var pawnId = (string)jsonObject["uid"];
        if (activeObjects.ContainsKey(pawnId))
        {
            var pawn = activeObjects[pawnId];
            pawn.transform.position = new Vector3((float) jsonObject["to_coord_x"], pawn.transform.position.y, (float) jsonObject["to_coord_z"]);
        }
    }
    
    private void PerformRotateObject(JObject jsonObject)
    {
        var responseJObj = new JObject();
        var pawnId = (string)jsonObject["uid"];
        if (activeObjects.ContainsKey(pawnId))
        {
            activeObjects[pawnId].transform.RotateAround(transform.position, Vector3.up, (float)jsonObject["to_rotate_y"]);
        }
    }
    
    private void PerformFlipObject(JObject jsonObject)
    {
        var pawnId = (string)jsonObject["uid"];
        if (activeObjects.ContainsKey(pawnId))
        {
            activeObjects[pawnId].transform.RotateAround(transform.position, Vector3.right, 180);
        }
    }
    
    /**
        Richiesta
        {
	        "operation": "object_released"
	        "uid": "254"
        }
        Risposta
        Caso positivo:
        {
	        "result": "success"
        }
        Caso negativo:
        {
	        "result": "failed"
        }
     */
    private void PerformReleaseObject(JObject jsonObject)
    {
        var pawnId = (string)jsonObject["uid"];
        if (activeObjects.ContainsKey(pawnId))
        {
            Destroy(activeObjects[pawnId]);
            activeObjects.Remove(pawnId);
        }
    }

    #endregion

    #region Polling Update Callback

    private void UpdateGame()
    {
        var result = zombicidePollingClient.SendGetUpdates(currentPlayer.Uid);
        if (result != null && !zombicidePollingClient.updatesSuspended)
        {
            #region UpdatePawns
            
            result.Pawns.ForEach(it => UpdateOrCreatePawn(it.Value));
            if (zombicidePollingClient.updatesSuspended)
                return;
            result.Players.ForEach(it => UpdateOrCreatePawn(it.Value));
            if (zombicidePollingClient.updatesSuspended)
                return;
            result.Zombies.ForEach(it => UpdateOrCreatePawn(it.Value));
            
            // delete pawns
            if (zombicidePollingClient.updatesSuspended)
                return;
            List<string> toRemovePawnKeys = new List<string>();
            foreach (var keyValuePair in activePawns)
            {
                // se NON ce n'è almeno uno (pawn, player o zombie che sia) con lo stesso uid
                if (result.Pawns.All(it => it.Key != keyValuePair.Key) &&
                    result.Players.All(it => it.Key != keyValuePair.Key) &&
                    result.Zombies.All(it => it.Key != keyValuePair.Key))
                {
                    // vuol dire che va rimosso
                    toRemovePawnKeys.Add(keyValuePair.Key);
                }
            }

            if (zombicidePollingClient.updatesSuspended)
                return;
            UnityMainThreadDispatcher.Instance().Enqueue(() =>
            {
                if (zombicidePollingClient.updatesSuspended)
                    return;
                toRemovePawnKeys.ForEach(key =>
                {
                    try
                    {
                        Destroy(activeObjects[key]);
                        activeObjects.Remove(key);
                        activePawns.Remove(key);
                    }
                    catch (Exception)
                    {
                        // ignore
                    }
                });
            });

            #endregion

            #region Update Cards

            // search for new cards
            if (zombicidePollingClient.updatesSuspended)
                return;
            UnityMainThreadDispatcher.Instance().Enqueue(() =>
            {
                if (zombicidePollingClient.updatesSuspended)
                    return;
                foreach (var keyValuePair in result.Cards)
                {
                    var userAlreadyHasGotCard = currentPlayer.EquipCards.ContainsKey(keyValuePair.Key);
                    if (!userAlreadyHasGotCard || !activeObjects.ContainsKey(keyValuePair.Key))
                    {
                        if (!userAlreadyHasGotCard)
                        {
                            currentPlayer.EquipCards.Add(keyValuePair.Key, keyValuePair.Value);
                        }
                        var newSpawnCard = Instantiate(cardObj, new Vector3(-2f, 25f, -16f), Quaternion.identity);
                        newSpawnCard.name = keyValuePair.Key;
                        newSpawnCard.GetComponent<Renderer>().material.mainTexture = equipCards[keyValuePair.Value];
                        newSpawnCard.GetComponent<Renderer>().material.mainTextureScale = new Vector2(-1, -1);
                        usedSpawnCards.Push(keyValuePair);
                        AddObjectWithoutPawn(keyValuePair.Key, newSpawnCard);
                    }
                }
            });
            
            // delete cards
            if (zombicidePollingClient.updatesSuspended)
                return;
            List<string> toRemoveCardKeys = new List<string>();
            foreach (var key in currentPlayer.EquipCards.Keys)
            {
                // se NON ce n'è almeno una carta con lo stesso uid
                if (result.Cards.All(item => item.Key != key))
                {
                    // vuol dire che va rimossa
                    toRemoveCardKeys.Add(key);
                    UnityEngine.Debug.Log("remove card: "+key);
                }
            }

            if (zombicidePollingClient.updatesSuspended)
                return;
            UnityMainThreadDispatcher.Instance().Enqueue(() =>
            {
                if (zombicidePollingClient.updatesSuspended)
                    return;
                toRemoveCardKeys.ForEach(key =>
                {
                    Destroy(activeObjects[key]);
                    activeObjects.Remove(key);
                    currentPlayer.EquipCards.Remove(key);
                });
            });

            #endregion
            
        }
    }

    private void UpdateOrCreatePawn(Pawn pawn)
    {
        // update objects
        if (activePawns.ContainsKey(pawn.Uid))
        {
            var foundPawn = activePawns[pawn.Uid];
            var newTransform = pawn;
            // if I'm not dragging and it's not a false loopback update
            if (!foundPawn.IsDragging &&
                (Math.Abs(foundPawn.OldCoordX - newTransform.CoordX) > 0.01 ||
                 Math.Abs(foundPawn.OldCoordZ - newTransform.CoordZ) > 0.01 ))
            {
                // update pawn's transform
                MoveObject(pawn.Uid, newTransform.CoordX, newTransform.CoordZ);
                RotateObject(pawn.Uid, newTransform.RotationY);
                FlipObject(pawn.Uid, newTransform.FlipX);
            }
        }
        // create objects
        else
        {
            CreateObject(pawn.Uid, pawn);
        }
    }

    #endregion

    #endregion

    public void SetPawnDragging(string uid, bool isDragging, Vector3 startPos = default)
    {
        Pawn pawn;
        if (activePawns.TryGetValue(uid, out pawn))
        {
            pawn.IsDragging = isDragging;
            if (isDragging)
            {
                pawn.OldCoordX = startPos.x;
                pawn.OldCoordY = startPos.y;
                pawn.OldCoordZ = startPos.z;
            }
        }
    }

    private void OnApplicationQuit()
    {
        StopZombicideClients();
    }

    public void StopZombicideClients()
    {
        zombicideClient?.StopClient();
        zombicidePollingClient?.StopClient();
    }
}