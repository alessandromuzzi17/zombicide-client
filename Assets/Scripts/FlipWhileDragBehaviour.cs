using System;
using UnityEngine;

public class FlipWhileDragBehaviour : MonoBehaviour
{
    private long lastDoorFlip;
    private const long MIN_DOOR_FLIP_DELTA = 2500000;

    private void Start()
    {
        lastDoorFlip = DateTime.Now.Ticks;
    }

    void OnMouseDrag()
    {
        if (Input.GetKey(KeyCode.F) && DateTime.Now.Ticks - lastDoorFlip > MIN_DOOR_FLIP_DELTA &&
            AppMain.instance.zombicideClient.SendObjectFlip(gameObject.name))
        {
            var currentRotation = transform.rotation.eulerAngles;
            transform.rotation = Quaternion.Euler((currentRotation.x+ 180)%360, currentRotation.y, currentRotation.z);
            lastDoorFlip = DateTime.Now.Ticks;
        }
    }
}