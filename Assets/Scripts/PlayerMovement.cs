﻿﻿using System;
 using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Debug = System.Diagnostics.Debug;

public class PlayerMovement : MonoBehaviour
{
    #region Public Properties

    public CharacterController controller;
    public float minX = -50f; 
    public float maxX = 50f;
    public float minZ = -5f; 
    public float maxZ = 50f; 
    public float speed = 2f;
    public float minFov = 15f;
    public float maxFov = 90f;
    public float sensitivity = 10f;

    #endregion

    private Camera mainCamera;

    private void Start()
    {
        mainCamera = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        if (mainCamera == default)
        {
            mainCamera = Camera.main;
        }
        // player movement
        float xAxis = Input.GetAxis("Horizontal");
        float zAxis = Input.GetAxis("Vertical");

        var controllerPosition = controller.transform.position;
        var newX = controllerPosition.x + xAxis * speed * Time.deltaTime;
        var newZ = controllerPosition.z + zAxis * speed * Time.deltaTime;
        if (newX < minX || newX > maxX)
            newX = 0;
        else
            newX -= controllerPosition.x;
        
        if (newZ < minZ || newZ > maxZ)
            newZ = 0;
        else
            newZ -= controllerPosition.z;
        
        var transform1 = transform;
        Vector3 move = transform1.right * newX
                       + transform1.forward * newZ;
        controller.Move(move);
        
        // fov movement
        float fov = mainCamera.fieldOfView;
        fov -= Input.GetAxis("Mouse ScrollWheel") * sensitivity;
        fov = Mathf.Clamp(fov, minFov, maxFov);
        mainCamera.fieldOfView = fov;
    }
}