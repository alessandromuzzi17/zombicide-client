using UnityEngine;

public class DragXAxis : MonoBehaviour
{
    public float minX;
    public float maxX;
    public float speed;

    private Vector3 distance;
    private Camera cam;

    private void Start()
    {
        cam = Camera.main;
    }

    void OnMouseDown()
    {
        distance = cam.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, cam.WorldToScreenPoint(transform.position).z)) - transform.position;
    }
  
    void OnMouseDrag()
    {
        Vector3 distance_to_screen = cam.WorldToScreenPoint(transform.position);
        Vector3 pos_move = cam.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, distance_to_screen.z ));
        var newX = pos_move.x - distance.x * speed;
        if (newX < minX)
            newX = minX;
        if (newX > maxX)
            newX = maxX;
        transform.position = new Vector3( newX , transform.position.y,  transform.position.z);
    }
}