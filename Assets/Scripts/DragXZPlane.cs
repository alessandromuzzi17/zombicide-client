using System;
using System.Threading.Tasks;
using UnityEngine;

public class DragXZPlane : MonoBehaviour
{
    private Vector3 distance;
    private Camera cam;
    private float yPos;
    private Vector3 startPos;
    private const float dragYOffset = 2f;
    private const float cardDragYOffset = 4f;
    private const float cardDragRotation = -60f;

    private void Start()
    {
        cam = Camera.main;
    }

    void OnMouseDown()
    {
        startPos = transform.position;
        AppMain.instance.SetPawnDragging(gameObject.name, true, startPos);
        distance = cam.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, cam.WorldToScreenPoint(transform.position).z)) - transform.position;
        if (transform.CompareTag("Card") || transform.CompareTag("card_wound"))
        {
            transform.position += new Vector3(0, cardDragYOffset, 0);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, 
                Quaternion.Euler(cardDragRotation, 0f, 0f),
                80);
            GetComponent<Rigidbody>().freezeRotation = true;
        }
        else
        {
            transform.position += new Vector3(0, dragYOffset, 0);
        }
        
        yPos = transform.position.y;
    }
  
    void OnMouseDrag()
    {
        Vector3 distance_to_screen = cam.WorldToScreenPoint(new Vector3(transform.position.x, yPos, transform.position.z));
        Vector3 pos_move = cam.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, distance_to_screen.z ));
        transform.position = new Vector3( pos_move.x - distance.x , yPos, pos_move.z - distance.z );
    }

    private void OnMouseUp()
    {
        AppMain.instance.SetPawnDragging(gameObject.name, false);
        if (transform.CompareTag("Card") || transform.CompareTag("card_wound"))
        {
            GetComponent<Rigidbody>().freezeRotation = false;
        }
        else
        {
            if (!AppMain.instance.zombicideClient.SendObjectMove(gameObject.name, startPos,transform.position))
            {
                UnityMainThreadDispatcher.Instance().Enqueue(() => transform.position = startPos);
            }
        }
    }
}