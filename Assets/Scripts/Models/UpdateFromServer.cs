using System.Collections.Generic;

namespace Models
{
    public class UpdateFromServer
    {
        public Dictionary<string, Pawn> Pawns { get; }
        public Dictionary<string, Player> Players { get; }
        public Dictionary<string, Zombie> Zombies { get; }
        public Dictionary<string, int> Cards { get; }

        public UpdateFromServer(Dictionary<string, Pawn> pawns, Dictionary<string, Player> players, Dictionary<string, Zombie> zombies, Dictionary<string, int> cards)
        {
            Pawns = pawns;
            Players = players;
            Zombies = zombies;
            Cards = cards;
        }
    }
}