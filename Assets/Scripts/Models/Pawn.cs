using UnityEngine;

public class Pawn
{
    public string Uid { get; set; }
    public float OldCoordX { get; set; }
    public float OldCoordY { get; set; }
    public float OldCoordZ { get; set; }
    public float CoordX { get; set; }
    public float CoordY { get; set; }
    public float CoordZ { get; set; }
    public float RotationY { get; set; }
    public float FlipX { get; set; }
    public bool IsDragging { get; set; }

    public Pawn()
    {
        IsDragging = false;
    }
    
    public Pawn(string uid) : this()
    {
        Uid = uid;
    }

    public Pawn(string uid, float coordX, float coordZ) : this(uid)
    {
        CoordX = coordX;
        CoordZ = coordZ;
    }

    public Pawn(GameObject gameObject) : this(gameObject.name)
    {
        UpdateWithGameObject(gameObject);
    }

    public void UpdateWithGameObject(GameObject gameObject)
    {
        var position = gameObject.transform.position;
        var rotation = gameObject.transform.rotation;
        CoordX = position.x;
        CoordZ = position.z;
        RotationY = rotation.y;
        FlipX = rotation.x;
    }
}