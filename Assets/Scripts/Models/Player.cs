using System.Collections.Generic;
using System.Net.Sockets;
using UnityEngine;

public class Player : Pawn
{
    public int Index { get; set; }
    public TcpClient TcpClient { get; set; }
    public Dictionary<string, int> EquipCards { get; set; } = new Dictionary<string, int>();
    public string Name { get; set; }
    public string Alias { get; set; }
    
    public string MacAddress { get; set; }

    public Player()
    {
        
    }
    
    public Player(string name, string userName, Vector3 pos, float rotY, float rotX) : base("-1")
    {
        Name = name;
        Alias = userName;
        CoordX = pos.x;
        CoordY = pos.y;
        CoordZ = pos.z;
        RotationY = rotY;
        FlipX = rotX;
    }
    
    public Player(string uid, int index, TcpClient tcpClient, string name) : base(uid)
    {
        Index = index;
        TcpClient = tcpClient;
        Name = name;
    }
}