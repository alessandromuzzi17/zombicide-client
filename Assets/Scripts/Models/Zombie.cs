using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

public class Zombie : Pawn
{
    [JsonProperty("type")]
    [JsonConverter(typeof(StringEnumConverter))]
    public AppMain.ZombieType Type { get; set; }
        
    public Zombie(string uid, AppMain.ZombieType type, float coordX, float coordZ) : base(uid, coordX, coordZ)
    {
        Type = type;
    }
}