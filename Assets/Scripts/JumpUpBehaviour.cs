﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpUpBehaviour : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        var oldPosition = other.transform.position;
        other.transform.position = new Vector3(oldPosition.x, 22f, oldPosition.z);
    }
}
