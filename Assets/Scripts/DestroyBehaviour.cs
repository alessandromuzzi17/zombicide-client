﻿using System;
using UnityEngine;

public class DestroyBehaviour : MonoBehaviour
{
    private long lastDoorRotation;
    private const long MIN_DOOR_FLIP_DELTA = 2500000;

    private void Start()
    {
        lastDoorRotation = DateTime.Now.Ticks;
    }

    void OnMouseDrag()
    {
        if (Input.GetKey(KeyCode.C) && DateTime.Now.Ticks - lastDoorRotation > MIN_DOOR_FLIP_DELTA)
        {
            DestroyObject();
        }
    }
    
    private void Update()
    {
        if (Input.GetMouseButtonDown(2))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform.gameObject.GetInstanceID() == this.gameObject.GetInstanceID())
                {
                    DestroyObject();
                }
            }   
        }
    }

    private void DestroyObject()
    {
        HFTDialog.MessageBox("Attenzione", "Sicuro di voler cancellare l'oggetto?", "Si", () =>
        {
            if (gameObject.CompareTag("player_female") ||
                gameObject.CompareTag("player_male") ||
                gameObject.CompareTag("card_wound"))
            {
                Destroy(gameObject);
            }
            else
            {
                if(AppMain.instance.zombicideClient.SendReleaseObject(gameObject.name))
                {
                    AppMain.instance.ReleaseObject(gameObject.name);
                    Destroy(gameObject);
                }
            }
        },"No", () => {});
    }
}
