using System;
using UnityEngine;

public class RotateWhileDragBehaviour : MonoBehaviour
{
    public float angle = 90;
    
    private long lastDoorRotation;
    private const long MIN_DOOR_FLIP_DELTA = 2500000;

    private void Start()
    {
        lastDoorRotation = DateTime.Now.Ticks;
    }

    void OnMouseDrag()
    {
        if (Input.GetKey(KeyCode.R) &&
            DateTime.Now.Ticks - lastDoorRotation > MIN_DOOR_FLIP_DELTA)
        {
            var currentRotation = gameObject.transform.rotation.eulerAngles;

            if (AppMain.instance.zombicideClient.SendObjectRotate(gameObject.name, currentRotation.y, currentRotation.y + angle))
            {
                transform.rotation = Quaternion.Euler(currentRotation.x, (currentRotation.y + angle)%360, currentRotation.z);
                lastDoorRotation = DateTime.Now.Ticks;
            }
        }
    }
}