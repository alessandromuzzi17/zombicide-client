using UnityEngine;

namespace Utils
{
    public class Utils
    {
        public static Vector3 GetRandomPos(float minX, float maxX, float minY, float maxY, float minZ, float maxZ)
        {
            System.Random r = new System.Random();
            return new Vector3((float)r.NextDouble() * (maxX-minX) + minX,
                               (float)r.NextDouble() * (maxY-minY) + minY,
                               (float)r.NextDouble() * (maxZ-minZ) + minZ);
        }

        public static Vector3 GetRandomPosInDefaultSpace()
        {
            return GetRandomPos(-20, 20, 22, 30, -15, -22.3f);
        }

        public static Vector2 GetRandomXZPos(float minX, float maxX, float minZ, float maxZ)
        {
            System.Random r = new System.Random();
            return new Vector3((float)r.NextDouble() * (maxX-minX) + minX,
                               (float)r.NextDouble() * (maxZ-minZ) + minZ);
        }
        
        public static float GetRandomYInDefaultSpace()
        {
            System.Random r = new System.Random();
            return (float)r.NextDouble() * (30-22) + 22;
        }
    }
}