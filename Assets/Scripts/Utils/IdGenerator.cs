namespace Utils
{
    namespace ZombicideServer.Utils
    {
        public class IdGenerator
        {
            private int counter = 0;

            public int GetNewID()
            {
                return counter++;
            }
        
            public string GetNewIDString()
            {
                return GetNewID().ToString();
            }
        }
    }
}