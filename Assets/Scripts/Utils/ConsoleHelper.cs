using System;
using System.Diagnostics;

namespace Utils
{
    public class ConsoleHelper
    {
        public static void WriteLine(string message)
        {
            if (AppMain.instance.isDebug)
            {
                Console.WriteLine(message);
                UnityEngine.Debug.Log(message);
            }
        }
        
        public static void Write(string message)
        {
            if (AppMain.instance.isDebug)
            {
                Console.Write(message);
                UnityEngine.Debug.Log(message);
            }
        }
    }
}