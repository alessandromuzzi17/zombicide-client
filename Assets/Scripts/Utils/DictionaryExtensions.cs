using System;
using System.Collections.Generic;

namespace Utils
{
    public static class DictionaryExtensions
    {
        public static void ForEach<T>(this IEnumerable<T> sequence, Action<T> action) {
            if (sequence == null) throw new ArgumentNullException("sequence");
            if (action == null) throw new ArgumentNullException("action");
            foreach(T item in sequence) 
                action(item);
        }

        //Return false to stop the loop
        public static void ForEach<T>(this IEnumerable<T> sequence, Func<T, bool> action) {
            if (sequence == null) throw new ArgumentNullException("sequence");
            if (action == null) throw new ArgumentNullException("action");

            foreach(T item in sequence) 
                if (!action(item))
                    return;
        }
    }
}