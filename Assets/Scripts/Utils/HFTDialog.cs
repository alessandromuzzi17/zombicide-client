using UnityEngine;
using System;

// example:
// HFTDialog.MessageBox("error", "Sorry but you're S.O.L", () => { Application.Quit() });

public class HFTDialog : MonoBehaviour {

    private enum HFTDialogType
    {
        Box,
        InputBox
    }
    
    Rect m_windowRect;
    Action m_action_ok;
    Action m_action_cancel;
    Action<string> m_action_input_ok;
    Action<string> m_action_input_cancel;
    string m_title;
    string m_msg;
    private string m_btn1;
    private string m_btn2;
    private HFTDialogType m_type;
    private string inputText = "";

    public static void MessageBox(string title, string msg, string btnOk, Action action, string btnCancel = "", Action actionCancel = null)
    {
        GameObject go = new GameObject("HFTDialog");
        HFTDialog dlg = go.AddComponent<HFTDialog>();
        dlg.Init(HFTDialogType.Box, title, msg, btnOk, action, btnCancel, actionCancel);
    }
    
    public static void MessageInputBox(string title, string msg, string btnOk, Action<string> actionOk, string btnCancel = "", Action<string> actionCancel = null)
    {
        GameObject go = new GameObject("HFTDialog");
        HFTDialog dlg = go.AddComponent<HFTDialog>();
        dlg.Init(HFTDialogType.InputBox, title, msg, btnOk, actionOk, btnCancel, actionCancel);
    }

    void Init(HFTDialogType dialogType, string title, string msg, string btn1, Action actionOk, string btn2, Action actionCancel)
    {
        m_title = title;
        m_msg = msg;
        m_btn1 = btn1; 
        m_action_ok = actionOk;
        m_btn2 = btn2;
        m_action_cancel = actionCancel;
        m_type = dialogType;
    }
    
    void Init(HFTDialogType dialogType, string title, string msg, string btn1, Action<string> inputActionOk, string btn2, Action<string> inputActionCancel)
    {
        m_title = title;
        m_msg = msg;
        m_btn1 = btn1; 
        m_action_input_ok = inputActionOk;
        m_btn2 = btn2;
        m_action_input_cancel = inputActionCancel;
        m_type = dialogType;
    }

    void OnGUI()
    {
        const int maxWidth = 340;
        const int maxHeight = 180;

        int width = Mathf.Min(maxWidth, Screen.width - 20);
        int height = Mathf.Min(maxHeight, Screen.height - 20);
        m_windowRect = new Rect(
            (Screen.width - width) / 2,
            (Screen.height - height) / 2,
            width,
            height);

        GUI.backgroundColor = new Color32(0, 0, 0, 255);
        m_windowRect = GUI.ModalWindow(0, m_windowRect, WindowFunc, m_title);
    }

    void WindowFunc(int windowID)
    {
        const int border = 10;
        const int width = 50;
        const int height = 25;
        const int spacing = 10;
        bool isInputBox = m_type == HFTDialogType.InputBox;
        float txtHeight = m_windowRect.height - border * 2 - height - spacing;
        if (isInputBox)
            txtHeight = txtHeight / 2 - 20;

        Rect l = new Rect(
            border,
            border + spacing,
            m_windowRect.width - border * 2,
            txtHeight);
        GUI.Label(l, m_msg);

        var inputBoxOffset = 0;
        if (isInputBox)
        {
            Rect inputRect = new Rect(
                border,
                border + spacing * 2 + l.height,
                m_windowRect.width - border * 2,
                (m_windowRect.height - border * 2 - height - spacing) / 2 -10);
            inputText = GUI.TextField(inputRect, inputText, 25);
        }

        Rect btnOk = new Rect(
            m_windowRect.width - width - border,
            m_windowRect.height - height - border + inputBoxOffset,
            width,
            height);

        Event e = Event.current;
        if (GUI.Button(btnOk, m_btn1) || (e != null && e.isKey && (e.keyCode == KeyCode.Return || e.keyCode == KeyCode.KeypadEnter)))
        {
            Destroy(this.gameObject);

            if (m_type == HFTDialogType.InputBox)
            {
                m_action_input_ok(inputText);
            }
            else
            {
                m_action_ok();
            }
        }

        if (m_action_cancel != null || m_action_input_cancel != null)
        {
            Rect btnCancel = new Rect(
                m_windowRect.width - width - border - btnOk.width - 15,
                m_windowRect.height - height - border + inputBoxOffset,
                width+10,
                height);
        
            if (GUI.Button(btnCancel, m_btn2))
            {
                Destroy(this.gameObject);
                
                if (m_type == HFTDialogType.InputBox)
                {
                    m_action_input_cancel(inputText);
                }
                else
                {
                    m_action_cancel();
                }
            }
        }
    }
}