using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using Newtonsoft.Json.Linq;
using Utils;

public class ZombicideServer
{
    #region Public Fields

    #region Server related data
        
    public readonly IPAddress IpAddress;
    public readonly int Port;
    public bool Running { get; private set; }
    public readonly int BufferSize = 2 * 1024;  // 2KB
        
    #endregion

    #endregion
        
    #region Private Fields

    private AppMain _appMain;
    private Action<TcpClient, string> _messagesCallback;
        
    // What listens in
    private TcpListener _listener;

    // types of clients connected
    private List<TcpClient> _clients = new List<TcpClient>();

    #endregion

    #region Constructor

    // Make a new TCP chat server, with our provided name
    public ZombicideServer(AppMain appMain, Action<TcpClient, string> messagesCallback, IPAddress ipAddress = null, int port = -1)
    {
        _appMain = appMain;
        _messagesCallback = messagesCallback;
        if (ipAddress == null)
        {
            IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());  
            ipAddress = ipHostInfo.AddressList.First(it => it.AddressFamily == AddressFamily.InterNetwork);
        }

        if (port == -1)
        {
            port = 11001;
        }
            
        // Set the basic data
        IpAddress = ipAddress;
        Port = port;
        Running = false;

        // Make the listener listen for connections on any network device
        _listener = new TcpListener(IpAddress, Port);
    }

    #endregion

    #region Run Method

    // Start running the server.  Will stop when `Shutdown()` has been called
    public void Run()
    {
        // Some info
        ConsoleHelper.WriteLine($"Starting the TCP Server on {IpAddress}:{Port}.");
        ConsoleHelper.WriteLine("Press Ctrl-C to shut down the server at any time.");

        // Make the server run
        _listener.Start();           // No backlog
        Running = true;

        // Main server loop
        while (Running)
        {
            // Check for new clients
            if (_listener.Pending())
                _handleNewConnection();

            // Do the rest
            _checkForDisconnects();
            _checkForNewMessages();

            // Use less CPU
            Thread.Sleep(10);
        }

        // Stop the server, and clean up any connected clients
        foreach (TcpClient v in _clients)
            _cleanupClient(v);

        _listener.Stop();

        // Some info
        ConsoleHelper.WriteLine("Server is shut down.");
    }

    #endregion

    #region Connections Handling Methods

    private void _handleNewConnection()
    {
        TcpClient newClient = _listener.AcceptTcpClient();      // Blocks

        // Modify the default buffer sizes
        newClient.SendBufferSize = BufferSize;
        newClient.ReceiveBufferSize = BufferSize;

        // Print some info
        EndPoint endPoint = newClient.Client.RemoteEndPoint;
        ConsoleHelper.WriteLine($"Handling a new client from {endPoint}...");

        _clients.Add(newClient);
        ConsoleHelper.WriteLine($"{endPoint} is a new client.");
    }
        
    // Checks if a socket has disconnected
    // Adapted from -- http://stackoverflow.com/questions/722240/instantly-detect-client-disconnection-from-server-socket
    private static bool _isDisconnected(TcpClient client)
    {
        try
        {
            Socket clientSocket = client.Client;
            return clientSocket.Poll(10 * 1000, SelectMode.SelectRead) && (clientSocket.Available == 0);
        }
        catch(SocketException)
        {
            // We got a socket error, assume it's disconnected
            return true;
        }
    }

    #endregion

    #region Requests Handling Methods

    // Sees if any of the clients have left the chat server
    private void _checkForDisconnects()
    {
        // Check the viewers first
        foreach (TcpClient client in _clients.ToArray())
        {
            if (_isDisconnected(client))
            {
                ConsoleHelper.WriteLine($"Client {client.Client.RemoteEndPoint} has left.");
                if (_appMain.connectedPlayers.Any(it => it.Value.TcpClient == client))
                {
                    try
                    {
                        _appMain.connectedPlayers.Remove(_appMain.connectedPlayers.FirstOrDefault(it => it.Value.TcpClient == client).Key);
                    }
                    catch (Exception)
                    {
                        // ignore
                    }
                }

                // cleanup on our end
                _clients.Remove(client);     // Remove from list
                _cleanupClient(client);
            }
        }
    }

    // See if any of our messengers have sent us a new message, put it in the queue
    private void _checkForNewMessages()
    {
        foreach (TcpClient client in _clients)
        {
            int messageLength = client.Available;
            if (messageLength > 0)
            {
                // there is one!  get it
                byte[] msgBuffer = new byte[messageLength];
                client.GetStream().Read(msgBuffer, 0, msgBuffer.Length);     // Blocks

                // handle received message
                var newMsg = Encoding.UTF8.GetString(msgBuffer);
                ConsoleHelper.WriteLine($"Received new message: {newMsg}");
                _messagesCallback(client, newMsg);
            }
        }
    }
        
    #endregion

    #region Send Method
        
    public void Send(TcpClient client, String data) {  
        // Encode the message
        byte[] msgBuffer = Encoding.UTF8.GetBytes(data);
        client.GetStream().Write(msgBuffer, 0, msgBuffer.Length);    // Blocks
    }  

    #endregion

    #region Shutdown And Cleanup Methods

    // If the server is running, this will shut down the server
    public void Shutdown()
    {
        Running = false;
        ConsoleHelper.WriteLine("Shutting down server");
    }
        
    // cleans up resources for a TcpClient
    private static void _cleanupClient(TcpClient client)
    {
        client.GetStream().Close();     // Close network stream
        client.Close();                 // Close client
    }

    #endregion
}