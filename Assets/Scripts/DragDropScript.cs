﻿// // using System.Collections;
// // using System.Collections.Generic;
// // using UnityEngine;
// //
// // public class DragDropScript : MonoBehaviour
// // {
// //
// //     //Initialize Variables
// //     GameObject getTarget;
// //     bool isMouseDragging;
// //     Vector3 offsetValue;
// //     Vector3 positionOfScreen;
// //
// //     // Use this for initialization
// //     void Start()
// //     {
// //
// //     }
// //
// //     void Update()
// //     {
// //
// //         //Mouse Button Press Down
// //         if (Input.GetMouseButtonDown(0))
// //         {
// //             RaycastHit hitInfo;
// //             getTarget = ReturnClickedObject(out hitInfo);
// //             if (getTarget != null)
// //             {
// //                 isMouseDragging = true;
// //                 //Converting world position to screen position.
// //                 positionOfScreen = Camera.main.WorldToScreenPoint(getTarget.transform.position);
// //                 offsetValue = getTarget.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, positionOfScreen.z));
// //             }
// //         }
// //
// //         //Mouse Button Up
// //         if (Input.GetMouseButtonUp(0))
// //         {
// //             isMouseDragging = false;
// //         }
// //
// //         //Is mouse Moving
// //         if (isMouseDragging)
// //         {
// //             //tracking mouse position.
// //             Vector3 currentScreenSpace = new Vector3(Input.mousePosition.x, Input.mousePosition.y, positionOfScreen.z);
// //
// //             //converting screen position to world position with offset changes.
// //             Vector3 currentPosition = Camera.main.ScreenToWorldPoint(currentScreenSpace) + offsetValue;
// //
// //             //It will update target gameobject's current postion.
// //             getTarget.transform.position = currentPosition;
// //         }
// //
// //
// //     }
// //
// //     //Method to Return Clicked Object
// //     GameObject ReturnClickedObject(out RaycastHit hit)
// //     {
// //         GameObject target = null;
// //         Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
// //         if (Physics.Raycast(ray.origin, ray.direction * 10, out hit))
// //         {
// //             target = hit.collider.gameObject;
// //         }
// //         return target;
// //     }
// //
// // }
// using System.Collections;
//
// using System.Collections.Generic;
//
// using UnityEngine;
//
//
//
// public class DragDropScript : MonoBehaviour
//
// {
//
//     private Vector3 mOffset;
//     private float mZCoord;
//     
//     void OnMouseDown()
//
//     {
//
//         mZCoord = Camera.main.WorldToScreenPoint(
//
//             gameObject.transform.position).z;
//         
//         // Store offset = gameobject world pos - mouse world pos
//
//         mOffset = gameObject.transform.position - GetMouseAsWorldPoint();
//
//     }
//
//
//
//     private Vector3 GetMouseAsWorldPoint()
//
//     {
//
//         // Pixel coordinates of mouse (x,y)
//
//         Vector3 mousePoint = Input.mousePosition;
//
//
//
//         // z coordinate of game object on screen
//
//         mousePoint.z = mZCoord;
//
//
//
//         // Convert it to world points
//
//         return Camera.main.ScreenToWorldPoint(mousePoint);
//
//     }
//
//
//
//     void OnMouseDrag()
//
//     {
//
//         transform.position = GetMouseAsWorldPoint() + mOffset;
//
//     }
//
// }