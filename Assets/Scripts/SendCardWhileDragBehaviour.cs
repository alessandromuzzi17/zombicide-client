using System;
using UnityEngine;

public class SendCardWhileDragBehaviour : MonoBehaviour
{
    private long lastDoorRotation;
    // expressed in ticks (note: 1 tick == 100ns)
    private const long MIN_SEND_CARD_DELTA = 10000000;
    private bool sendBlocked = false;

    private void Start()
    {
        lastDoorRotation = DateTime.Now.Ticks;
    }

    void OnMouseDrag()
    {
        if (Input.GetKey(KeyCode.I) &&
            DateTime.Now.Ticks - lastDoorRotation > MIN_SEND_CARD_DELTA)
        {
            if (!sendBlocked)
            {
                HFTDialog.MessageInputBox("Invia carta", "A chi vuoi inviare la carta?\nImmetti un numero da 0 a 9:", "Ok",
                    text =>
                    {
                        int selectedNumber;
                        if (int.TryParse(text, out selectedNumber))
                        {
                            if (AppMain.instance.zombicideClient.SendCard(AppMain.instance.currentPlayer.Uid, selectedNumber, Int32.Parse(gameObject.name)))
                            {
                                Destroy(gameObject);
                            }
                        }
                        else
                        {
                            HFTDialog.MessageBox("Attenzione", "Il valore immesso non è un numero valido.", "Ok", () => { });
                        }
                        lastDoorRotation = DateTime.Now.Ticks;
                    }, "Annulla", text => {});
            }
            else
            {
                HFTDialog.MessageBox("Attenzione", "Attendere prego.\nInvio carta in corso...", "Ok", () => { });
            }
        }
    }
}